test( "init", function() {

    var engine = app.engine.make();
    engine.init(4)
    assertEquals(4,engine.getBoard().length);
    assertEquals(false,engine.getBoard()[1][2]);

});


test( "populate", function() {

    var engine = app.engine.make();
    engine.init(3);
    engine.populate([ [0, 1], [1, 1], [2, 0]] );
    assertEquals(false,engine.getBoard()[0][0]);
    assertEquals(true,engine.getBoard()[0][1]);
    assertEquals(false,engine.getBoard()[0][2]);
    assertEquals(false,engine.getBoard()[1][0]);
    assertEquals(true,engine.getBoard()[1][1]);
    assertEquals(false,engine.getBoard()[1][2]);
    assertEquals(true,engine.getBoard()[2][0]);
    assertEquals(false,engine.getBoard()[2][1]);
    assertEquals(false,engine.getBoard()[2][2]);

});

test( "getCellValue", function() {

    var engine = app.engine.make();
    engine.init(3);
    engine.populate([ [0, 1], [1, 1], [2, 0]] );
    assertEquals(true, engine.getCellValue(engine.state.board, 1, 1));
    assertEquals(false, engine.getCellValue(engine.state.board, 0, 0));
    assertEquals(false, engine.getCellValue(engine.state.board, -7, 5));
    assertEquals(false, engine.getCellValue(engine.state.board, 1, 3));

});

test( "calculateNextStep", function() {

    var engine = app.engine.make();
    engine.init(5);
    engine.populate([ [1, 1], [2, 2], [3, 1]] );

    assertEquals(true, engine.calculateNextStep(engine.state.board, 2, 1));
    assertEquals(false, engine.calculateNextStep(engine.state.board, 1  , 1));
    assertEquals(true, engine.calculateNextStep(engine.state.board, 2, 2));
    assertEquals(false, engine.calculateNextStep(engine.state.board, 3, 1));


});



test( "step", function() {

    var engine = app.engine.make();
    engine.init(5);
    engine.populate([ [1, 1], [2, 2], [3, 1] ]);
    engine.step();

    assertEquals(true, engine.getCellValue(engine.state.board, 2, 1));
    assertEquals(false, engine.getCellValue(engine.state.board, 1  , 1));
    assertEquals(true, engine.getCellValue(engine.state.board, 2, 2));
    assertEquals(false, engine.getCellValue(engine.state.board, 3, 1));

    engine.step();
    assertEquals(false, engine.getCellValue(engine.state.board, 2, 1));
    assertEquals(false, engine.getCellValue(engine.state.board, 2, 2));

});




