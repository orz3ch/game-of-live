app.engine = {

    make: function () {

        return {
            state: {

            },

            init: function (boardSize) {
                app.engine.state = {};

                var i = 0,
                    j = 0;
                var board = new Array(boardSize)

                for (i = 0; i < boardSize; i++) {
                    board[i] = new Array(boardSize);
                }

                this.state.size = boardSize;
                this.clear(board);
                this.state.board = board;
            },

            clear: function (board) {
                var i = 0,
                    j = 0;

                for (i = 0; i < this.state.size; i++) {
                    for (j = 0; j < this.state.size; j++) {
                        board[i][j] = false
                    }
                }
            },

            populate: function (liveCels) {
                var that = this;

                that.clear(this.state.board);

                _.each(liveCels, function(el) {
                    that.state.board[el[0]][el[1]] = true;
                })
            },

            step: function () {
                var oldBoard = this.state.board;
                this.init(this.state.size);


                var i = 0,
                    j = 0;

                for (i = 0; i < this.state.size; i++) {
                    for (j = 0; j < this.state.size; j++) {
                        this.state.board[i][j] = this.calculateNextStep(oldBoard, i, j)
                    }
                }



            },


            calculateNextStep : function(board, x, y) {

                var i = 0,
                    j = 0,
                    sum = 0;

                for (i = x - 1; i <= x + 1; i++) {
                    for (j = y - 1; j <= y + 1; j++) {

                        if ( x == i && y == j ) continue;

                        if(this.getCellValue(board, i, j)) {
                            sum ++;
                        }
                    }
                }

                return this.getCellValue(board, x, y) ? (sum == 3 || sum == 2) : (sum == 3);
            },

            getBoard: function() {
                return this.state.board;
            },

            getCellValue : function (board, x, y) {
                if (x >= this.state.size || x < 0) return false;
                if (y >= this.state.size || y < 0) return false;
                return board[x][y];
            }


        }
    }

}
